# This program pulls information from your myWSU classes page and creates
# Events in your Google Calender
# Coded by Reed Schimmel & Amr Metwaly
# Started 1/25/2017

# import needed libraries
from bs4 import BeautifulSoup
from urllib.request import urlopen
import lxml
import re

# myWSU_URL = "https://mywsu.wichita.edu/"
# myWSUclasses = "https://mywsu.wichita.edu/web/home-community/myclasses"
# tempURL = "https://ssbprod.wichita.edu/PROD/bwskcrse.P_CrseSchdDetl"

# URL = input("Paste the URL of your Concise Student Schedule page:")
# print(tempURL)
# print("downloading html...")
# html = urlopen(URL)
# print(html.read())
# TODO figure out how to log in to pull the html


def main():  
  # Open a file for writing and create it if it doesn't exist
  f = open("coinciseSchd.txt","w+")
  
  # Open the file for appending text to the end
#  f = open("textfile.txt","a+")

  # write some lines of data to the file
html =""
soup = BeautifulSoup(open(ConciseStudentSchedule.html), "lxml")
table = soup.findAll("table","datadisplaytable")
table = table[1]
rows=list()
for row in table.findAll("tr"):
   rows.append(row)
   f.write(rows, "\r\n" % (i+1))
  
  # close the file when done
   f.close()
  
  # Open the file back up and read the contents
f = open("textfile.txt","r")
if f.mode == 'r': # check to make sure that the file was opened
    # use the read() function to read the entire file
#     contents = f.read()
#     print contents
    
    fl = f.readlines() # readlines reads the individual lines into a list
    for lines in fl:
     print (lines)
#htmlString = """
#<table  CLASS="datadisplaytable" SUMMARY="Display course details for a student.,BORDER = 1,"WIDTH="100%">
#<tr>
#<th CLASS="ddheader" scope="col" ><ACRONYM title = "Course Reference Number">CRN</ACRONYM></th>
#<th CLASS="ddheader" scope="col" >Course</th>
#<th CLASS="ddheader" scope="col" >Title</th>
#<th CLASS="ddheader" scope="col" >Campus</th>
#<th CLASS="ddheader" scope="col" >Credits</th>
#<th CLASS="ddheader" scope="col" >Level</th>
#<th CLASS="ddheader" scope="col" >Start Date</th>
#<th CLASS="ddheader" scope="col" >End Date</th>
#<th CLASS="ddheader" scope="col" >Days</th>
#<th CLASS="ddheader" scope="col" >Time</th>
#<th CLASS="ddheader" scope="col" >Location</th>
#<th CLASS="ddheader" scope="col" >Instructor</th>
#</tr>
#<tr>
#<td CLASS="dddefault">21580</td>
#<td CLASS="dddefault">AE 223 0</td>
#<td CLASS="dddefault">Statics</td>
#<td CLASS="dddefault">Main</td>
#<td CLASS="dddefault">    3.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">MW</td>
#<td CLASS="dddefault">4:10 pm - 5:25 pm</td>
#<td CLASS="dddefault">Neff Hall 210</td>
#<td CLASS="dddefault">Matheswaran</td>
#</tr>
#<tr>
#<td CLASS="dddefault">21175</td>
#<td CLASS="dddefault">BIOL 210 0</td>
#<td CLASS="dddefault">General Biology I</td>
#<td CLASS="dddefault">Main</td>
#<td CLASS="dddefault">    4.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">MWF</td>
#<td CLASS="dddefault">9:30 am - 10:20 am</td>
#<td CLASS="dddefault">R.D. Hubbard Hall 211</td>
#<td CLASS="dddefault">Bousfield</td>
#</tr>
#tr>
#<td CLASS="dddefault">22677</td>
#<td CLASS="dddefault">BIOL 210L 0</td>
#<td CLASS="dddefault">General Biology I Lab</td>
#<td CLASS="dddefault">Main</td>
#<td CLASS="dddefault">    0.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">T</td>
#<td CLASS="dddefault">2:00 pm - 4:50 pm</td>
#<td CLASS="dddefault">R.D. Hubbard Hall 536</td>
#<td CLASS="dddefault">Schouten</td>
#</tr>
#<tr>
#<td CLASS="dddefault">21963</td>
#<td CLASS="dddefault">DANC 130V 0</td>
#<td CLASS="dddefault">Hip Hop  I</td>
#<td CLASS="dddefault">Main</td>
#<td CLASS="dddefault">    2.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">MW</td>
#<td CLASS="dddefault">2:00 pm - 3:15 pm</td>
#<td CLASS="dddefault">Heskett Center 141</td>
#<td CLASS="dddefault">Pollard</td>
#</tr>
#<tr>
#<td CLASS="dddefault">22607</td>
#<td CLASS="dddefault">HS 570 0</td>
#<td CLASS="dddefault">Neuroscience for Health Professionals: Peripheral Nervous System</td>
#<td CLASS="dddefault">Off-campus</td>
#<td CLASS="dddefault">    1.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">&nbsp;</td>
#<td CLASS="dddefault"><ABBR title = "To Be Announced">TBA</ABBR></td>
#<td CLASS="dddefault">ONLINE </td>
#<td CLASS="dddefault">Pitetti</td>
#</tr>
#<tr>
#<td CLASS="dddefault">21420</td>
#<td CLASS="dddefault">MATH 555 0</td>
#<td CLASS="dddefault">Differential Equations I</td>
#<td CLASS="dddefault">Main</td>
#<td CLASS="dddefault">    3.000</td>
#<td CLASS="dddefault">UG</td>
#<td CLASS="dddefault">Jan 17, 2017</td>
#<td CLASS="dddefault">May 11, 2017</td>
#<td CLASS="dddefault">MWF</td>
#<td CLASS="dddefault">11:30 am - 12:20 pm</td>
#<td CLASS="dddefault">Elliott Hall 112</td>
#<td CLASS="dddefault">Ma</td>
#</tr>
#<tr>
#<td CLASS="dddefault">&nbsp;</td>
#<td CLASS="dddefault">&nbsp;</td>
#<td CLASS="dddefault">&nbsp;</td>
#<td CLASS="dddefault"><b>Total Credits:</b></td>
#<td CLASS="dddefault"><b>   13.000</b></td>
#</tr>
#</table>
#"""
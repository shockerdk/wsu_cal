# This program pulls information from your myWSU classes page and creates
# Events in your Google Calender
# Coded by Reed Schimmel
# Started 1/25/2017

# Refer to the Python quickstart on how to setup the environment:
# https://developers.google.com/google-apps/calendar/quickstart/python
# Change the scope to 'https://www.googleapis.com/auth/calendar' and delete any
# stored credentials.


#Course Title 
timeZone = "America/Chicago"# static for now
for lecture in classes:

  start = "2015-05-28T09:00:00-07:00"
  end = "2015-05-28T17:00:00-07:00"
  location = "idk"
  summary = "Title"
  description = "you've got class"

  event = {
    'summary': '{summary}'.format(**vars()),
    'location': '{location}'.format(**vars()),
    'description': 'description'.format(**vars()),
    'start': {
      'dateTime': '{start}'.format(**vars()),
      'timeZone': '{timeZone}'.format(**vars()),
    },
    'end': {
      'dateTime': '{end}'.format(**vars()),
      'timeZone': '{timeZone}'.format(**vars()),
    },
    'recurrence': [ ##TODO recurrence 
      'RRULE:FREQ=DAILY;COUNT=2'
    ],
    'attendees': [
      {'email': 'lpage@example.com'},
      {'email': 'sbrin@example.com'},
    ],
    'reminders': {
      'useDefault': False,
      'overrides': [
        {'method': 'popup', 'minutes': 30},
      ],
    },
  }

print(event)
event = service.events().insert(calendarId='primary', body=event).execute()
print 'Event created: %s' % (event.get('htmlLink'))